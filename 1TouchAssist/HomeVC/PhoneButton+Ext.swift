//
//  PhoneButton+Ext.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/15/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

let ud = UserDefaults.standard


extension HomeVC {
    
    func editRequest(key: String) {
        if ud.string(forKey: key) != nil {
            let edit = Edit.shared
            edit.contactName = ud.string(forKey: "name"+numberSelected)
            edit.contactNumber = ud.string(forKey: "phone"+numberSelected)
            edit.image = ud.data(forKey: "image"+numberSelected)
            edit.editing = true
            UIView.animate(withDuration: 0.35) {
                self.phoneView.alpha = 0
                self.labelText.alpha = 1
            }
            let sb = UIStoryboard(name: "AddPhoneDetailsVC", bundle: nil)
            guard let vc = sb.instantiateInitialViewController() else { return }
            present(vc, animated: true, completion: nil)
        } else {
            UIView.animate(withDuration: 0.35) {
                self.phoneView.alpha = 0
                self.labelText.alpha = 1
            }
            let sb = UIStoryboard(name: "AddPhoneDetailsVC", bundle: nil)
            guard let vc = sb.instantiateInitialViewController() else { return }
            present(vc, animated: true, completion: nil)
        }
    }
    
    fileprivate func checkPhone(key: String, button: UIButton) {
        if ud.string(forKey: key) != nil {
            guard let phone = ud.string(forKey: key) else { return }
            makeCall(with: phone, vc: self)
        } else {
            UIView.animate(withDuration: 0.35) {
                self.phoneView.alpha = 0
                self.labelText.alpha = 1
            }
            let sb = UIStoryboard(name: "AddPhoneDetailsVC", bundle: nil)
            guard let vc = sb.instantiateInitialViewController() else { return }
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func phone1(_ sender: UIButton) {
        numberSelected = "1"
        checkPhone(key: "phone1", button: sender)
    }
    @IBAction func phone2(_ sender: UIButton) {
        numberSelected = "2"
        checkPhone(key: "phone2", button: sender)
    }
    @IBAction func phone3(_ sender: UIButton) {
        numberSelected = "3"
        checkPhone(key: "phone3", button: sender)
    }
    @IBAction func phone4(_ sender: UIButton) {
        numberSelected = "4"
        checkPhone(key: "phone4", button: sender)
    }
    @IBAction func phone5(_ sender: UIButton) {
        numberSelected = "5"
        checkPhone(key: "phone5", button: sender)
    }
    @IBAction func phone6(_ sender: UIButton) {
        numberSelected = "6"
        checkPhone(key: "phone6", button: sender)
    }
}
