//
//  MusicButton+Ext.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/15/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

extension HomeVC {
    
    @IBAction func appleMusic(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "music://")!) {
            UIApplication.shared.open(URL(string: "music://")!, options: [:], completionHandler: nil)
        } else {
            let ac = UIAlertController.init(title: "Error", message: "Apple Music Must Not Be Installed", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
            ac.addAction(ok)
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    @IBAction func youtube(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "Youtube://")!) {
            UIApplication.shared.open(URL(string: "Youtube://")!, options: [:], completionHandler: nil)
        } else {
            let ac = UIAlertController.init(title: "Error", message: "Youtube Must Not Be Installed", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
            ac.addAction(ok)
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    @IBAction func pandora(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "pandora://")!) {
            UIApplication.shared.open(URL(string: "pandora://")!, options: [:], completionHandler: nil)
        } else {
            let ac = UIAlertController.init(title: "Error", message: "Pandora Must Not Be Installed", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
            ac.addAction(ok)
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    @IBAction func podcasts(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "podcasts://")!) {
            UIApplication.shared.open(URL(string: "podcasts://")!, options: [:], completionHandler: nil)
        } else {
            let ac = UIAlertController.init(title: "Error", message: "Podcasts Must Not Be Installed", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
            ac.addAction(ok)
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    @IBAction func spotify(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "spotify://")!) {
            UIApplication.shared.open(URL(string: "spotify://")!, options: [:], completionHandler: nil)
        } else {
            let ac = UIAlertController.init(title: "Error", message: "Spotify Must Not Be Installed", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
            ac.addAction(ok)
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    @IBAction func ytmusic(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "youtubemusic://")!) {
            UIApplication.shared.open(URL(string: "youtubemusic://")!, options: [:], completionHandler: nil)
        } else {
            let ac = UIAlertController.init(title: "Error", message: "Youtube Music Must Not Be Installed", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
            ac.addAction(ok)
            self.present(ac, animated: true, completion: nil)
        }
    }
    
}
