//
//  HomeVC+Ext.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/17/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

extension HomeVC {
    @objc func phone1Func() {
        numberSelected = "1"
        editRequest(key: "phone"+numberSelected)
    }
    @objc func phone2Func() {
        numberSelected = "2"
        editRequest(key: "phone"+numberSelected)
    }
    @objc func phone3Func() {
        numberSelected = "3"
        editRequest(key: "phone"+numberSelected)
    }
    @objc func phone4Func() {
        numberSelected = "4"
        editRequest(key: "phone"+numberSelected)
    }
    @objc func phone5Func() {
        numberSelected = "5"
        editRequest(key: "phone"+numberSelected)
    }
    @objc func phone6Func() {
        numberSelected = "6"
        editRequest(key: "phone"+numberSelected)
    }
}
