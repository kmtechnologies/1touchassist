//
//  ViewController.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/12/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMobileAds

var launchCompleted = false

class HomeVC: UIViewController {
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    //phone outlets
    @IBOutlet weak var phone1Btn: UIButton!
    @IBOutlet weak var phone2Btn: UIButton!
    @IBOutlet weak var phone3Btn: UIButton!
    @IBOutlet weak var phone4Btn: UIButton!
    @IBOutlet weak var phone5Btn: UIButton!
    @IBOutlet weak var phone6Btn: UIButton!
    //
    
    //launch screen animation
    @IBOutlet weak var blueBG: UIView!
    @IBOutlet weak var launchIcon: UIView!
    //

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var labelText: UITextView!
    @IBOutlet weak var musicView: UIView!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var musicLabel: UILabel!
    @IBOutlet weak var todoLabel: UILabel!
    @IBOutlet weak var mapsLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    
    func addEdit() {
        let long1 = UILongPressGestureRecognizer(target: self, action: #selector(phone1Func))
        let long2 = UILongPressGestureRecognizer(target: self, action: #selector(phone2Func))
        let long3 = UILongPressGestureRecognizer(target: self, action: #selector(phone3Func))
        let long4 = UILongPressGestureRecognizer(target: self, action: #selector(phone4Func))
        let long5 = UILongPressGestureRecognizer(target: self, action: #selector(phone5Func))
        let long6 = UILongPressGestureRecognizer(target: self, action: #selector(phone6Func))
        phone1Btn.addGestureRecognizer(long1)
        phone2Btn.addGestureRecognizer(long2)
        phone3Btn.addGestureRecognizer(long3)
        phone4Btn.addGestureRecognizer(long4)
        phone5Btn.addGestureRecognizer(long5)
        phone6Btn.addGestureRecognizer(long6)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //test id ca-app-pub-3940256099942544/2934735716
        //real id ca-app-pub-3940256099942544/1712485313
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (didAllow, error) in
            
        }
        if ud.bool(forKey: "tabbarLabels") == false {
            phoneLabel.isHidden = true
            musicLabel.isHidden = true
            todoLabel.isHidden = true
            mapsLabel.isHidden = true
            settingsLabel.isHidden = true
        } else {
            phoneLabel.isHidden = false
            musicLabel.isHidden = false
            todoLabel.isHidden = false
            mapsLabel.isHidden = false
            settingsLabel.isHidden = false
        }
        addEdit()
        self.phoneView.isHidden = true
        self.phoneView.alpha = 0
        self.musicView.alpha = 0
        blurView.alpha = 0.85
        if launchCompleted == false {
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.animateLaunch()
            }
        } else {
            blueBG.isHidden = true
            launchIcon.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        checkPhoneStatus()
    }
    
    fileprivate func updatePhoneBtn(key: String, button: UIButton) {
        if ud.string(forKey: key) != nil {
            guard let name = ud.string(forKey: key) else { return }
            button.setTitle(name, for: .normal)
        } else {
            button.setTitle("Add?", for: .normal)
        }
    }
    
    func getImageFromDefaults(key: String, button: UIButton) {
        if ud.data(forKey: key) != nil {
            guard let imageData = ud.data(forKey: key) else { return }
            guard let image = UIImage(data: imageData) else { return }
            button.clipsToBounds = true
            button.contentMode = .scaleAspectFit
            button.setBackgroundImage(image, for: .normal)
        } else {
            button.setBackgroundImage(nil, for: .normal)
        }
    }
    
    func checkPhoneStatus() {
        updatePhoneBtn(key: "name1", button: phone1Btn)
        updatePhoneBtn(key: "name2", button: phone2Btn)
        updatePhoneBtn(key: "name3", button: phone3Btn)
        updatePhoneBtn(key: "name4", button: phone4Btn)
        updatePhoneBtn(key: "name5", button: phone5Btn)
        updatePhoneBtn(key: "name6", button: phone6Btn)
        getImageFromDefaults(key: "image1", button: phone1Btn)
        getImageFromDefaults(key: "image2", button: phone2Btn)
        getImageFromDefaults(key: "image3", button: phone3Btn)
        getImageFromDefaults(key: "image4", button: phone4Btn)
        getImageFromDefaults(key: "image5", button: phone5Btn)
        getImageFromDefaults(key: "image6", button: phone6Btn)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func phoneClicked(_ sender: Any) {
        checkPhoneStatus()
        UIView.animate(withDuration: 0.35) {
            if self.musicView.alpha == 1 {
                self.musicView.alpha = 0
            }
            if self.phoneView.alpha == 1 {
                self.phoneView.alpha = 0
                self.labelText.alpha = 1
            } else {
                UIView.animate(withDuration: 0.35, animations: {
                    self.labelText.alpha = 0
                    self.phoneView.isHidden = false
                    self.phoneView.alpha = 1
                })
            }
        }
    }
    
    @IBAction func musicClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.35) {
            if self.phoneView.alpha == 1 {
                self.phoneView.alpha = 0
            }
            if self.musicView.alpha == 1 {
                self.musicView.alpha = 0
                self.labelText.alpha = 1
            } else {
                UIView.animate(withDuration: 0.35, animations: {
                    self.labelText.alpha = 0
                    self.musicView.isHidden = false
                    self.musicView.alpha = 1
                })
            }
        }
    }
    
    @IBAction func todoClicked(_ sender: Any) {
        let sb = UIStoryboard.init(name: "TodoVC", bundle: nil)
        let vc = sb.instantiateInitialViewController()
        present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func mapsClicked(_ sender: Any) {
        guard let url = URL(string: "maps://") else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            let ac = UIAlertController.init(title: "Error", message: "Maps Must Not Be Installed", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
            ac.addAction(ok)
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    @IBAction func settingsClicked(_ sender: Any) {
        let sb = UIStoryboard.init(name: "SettingsVC", bundle: nil)
        let vc = sb.instantiateInitialViewController()
        present(vc!, animated: true, completion: nil)
    }
    
}
extension HomeVC {
    func animateLaunch() {
        launchCompleted = true
        UIView.animate(withDuration: 1, animations: {
            self.launchIcon.transform = CGAffineTransform(scaleX: 200, y: 200)
            self.launchIcon.alpha = 0
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                UIView.animate(withDuration: 0.5, animations: {
                    self.blueBG.alpha = 0
                })
            })
        })
    }
}
