//
//  TodoDetailVC.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/16/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

class TodoDetailVC: UIViewController {


    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var titleTextfield: UITextField!
    @IBOutlet weak var colorSelect: UISegmentedControl!
    @IBOutlet weak var messageView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleTextfield.delegate = self
        messageView.delegate = self
        title = "Details/Edit"
        messageView.layer.borderColor = UIColor.white.cgColor
        messageView.layer.borderWidth = 1
        messageView.layer.cornerRadius = 10
        titleTextfield.text = selectedItem!.title
        datePicker.setValue(UIColor.white, forKey: "textColor")
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(handleSave))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Home", style: .done, target: self, action: #selector(handleHome))
        datePicker.minimumDate = Date()
        datePicker.date = selectedItem!.date
        switch selectedItem!.color {
        case "Blue":
            colorSelect.selectedSegmentIndex = 0
        case "Red":
            colorSelect.selectedSegmentIndex = 1
        case "Green":
            colorSelect.selectedSegmentIndex = 2
        case "Yellow":
            colorSelect.selectedSegmentIndex = 3
        default:
            colorSelect.selectedSegmentIndex = 0
        }
        messageView.text = selectedItem!.message
        
    }
    
    @objc func handleHome() {
        self.performSegue(withIdentifier: "toTable", sender: self)
    }
    
    
    
    @objc func handleSave() {
        jsonTodoArray[selectedIndex!].color = colorSelect.titleForSegment(at: colorSelect.selectedSegmentIndex)!
        jsonTodoArray[selectedIndex!].title = titleTextfield.text!
        jsonTodoArray[selectedIndex!].date = datePicker.date
        jsonTodoArray[selectedIndex!].message = messageView.text
        saveJson()
        self.view.endEditing(true)
        navigationItem.rightBarButtonItem?.title = "Saved"
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(self.handleSave))
        }
    }
    
    @objc func keyboardWillChange(notification: Notification) {
        
        guard let rect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name ==  UIResponder.keyboardWillChangeFrameNotification{
            view.frame.origin.y = -rect.height
        } else {
            view.frame.origin.y = 0
        }
        
    }

    @IBAction func newColor(_ sender: UISegmentedControl) {
        UIView.animate(withDuration: 0.35) {
            switch sender.titleForSegment(at: sender.selectedSegmentIndex) {
            case "Red" :
                sender.tintColor = UIColor(named: "todoRed")
            case "Green":
                sender.tintColor = UIColor(named: "todoGreen")
            case "Purple":
                sender.tintColor = UIColor(named: "todoPurple")
            case "Blue":
                sender.tintColor = UIColor(named: "todoBlue")
            default:
                sender.tintColor = .white
            }
        }
    }
}
extension TodoDetailVC: UITextFieldDelegate, UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        addObservers()
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        removeObservers()
    }
    
    func addObservers() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func removeObservers() {
        let center = NotificationCenter.default
        center.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        center.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        center.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
}
