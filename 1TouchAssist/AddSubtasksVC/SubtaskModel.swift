//
//  SubtaskModel.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/18/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

struct Subtask: Codable {
    var title: String
    var completed: Bool
}
