//
//  AddSubtasksVC.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/18/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

var reloadIndex: Int?

var publicIndexPath: IndexPath?

var subtaskIndex: Int?

class AddSubtasksVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.isHidden = true
        checkEdit()
        mainView.layer.cornerRadius = 10
        mainView.layer.borderColor = UIColor.white.cgColor
        mainView.layer.borderWidth = 1
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        let edit = EditSubtask.shared
        edit.title = nil
        edit.editing = false
    }
    
    
    @IBAction func textChnaged(_ sender: Any) {
        if titleText.text != "" {
            addButton.isHidden = false
        } else {
            addButton.isHidden = true
        }
    }
    
    func checkEdit() {
        let edit = EditSubtask.shared
        if edit.editing == true {
            addButton.isHidden = false
            titleText.text = edit.title!
        } else {
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "back" {
            guard let section = selectedIndex else { return }
            reloadIndex = section
            guard let row = subtaskIndex else { return }
            guard let text = titleText.text else { return }
            publicIndexPath = IndexPath(row: row, section: section)
            let task = Subtask(title: text, completed: false)
            let edit = EditSubtask.shared
            if edit.editing == false {
                jsonTodoArray[section].subtasks.append(task)
                saveJson()
                edit.title = nil
                edit.editing = false
            } else {
                jsonTodoArray[section].subtasks[row - 1].title = text
                saveJson()
                edit.editing = false
            }
        }
    }
    
    @IBAction func addSubtask(_ sender: Any) {
        print("testing")
        performSegue(withIdentifier: "back", sender: self)
    }
}
