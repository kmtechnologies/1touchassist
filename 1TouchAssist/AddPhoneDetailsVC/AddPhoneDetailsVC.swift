//
//  AddPhoneDetailsVC.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/17/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

var numberSelected: String!

class AddPhoneDetailsVC: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var contactName: UITextField!
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkEdit()
        mainView.layer.cornerRadius = 10
        mainView.layer.borderColor = UIColor.white.cgColor
        mainView.layer.borderWidth = 1
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        imageView.addGestureRecognizer(tap)
    }
    
    func checkEdit() {
        let edit = Edit.shared
        if edit.editing == true {
            contactName.text = edit.contactName
            contactNumber.text = edit.contactNumber
            guard let image = UIImage(data: edit.image!) else { return }
            imageView.image = image
            addButton.setTitle("Save", for: .normal)
            cancelButton.setTitle("Delete", for: .normal)
            cancelButton.setTitleColor(.red, for: .normal)
            cancelButton.addTarget(self, action: #selector(deleteContact), for: .touchUpInside)
        }
    }
    
    @objc func deleteContact() {
        ud.removeObject(forKey: "name"+numberSelected)
        ud.removeObject(forKey: "phone"+numberSelected)
        ud.removeObject(forKey: "image"+numberSelected)
        let alert = SweetAlert()
        let ac = alert.showAlert("Deleted", subTitle: "The contact has been deleted!", style: .success, buttonTitle: "Okay")
        self.present(ac, animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        let edit = Edit.shared
        edit.contactName = nil
        edit.contactNumber = nil
        edit.editing = false
        edit.image = nil
        addButton.setTitle("Add", for: .normal)
    }
    
    @objc func handleTap() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }

    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func saveImagetoDefaults(with image: UIImage) {
        guard let imageData = image.pngData() else { return }
        ud.set(imageData, forKey: "image"+numberSelected)
    }
    
    func savePhoneDetails(name: String, number: String) {
        guard let image = imageView.image else { return }
        saveImagetoDefaults(with: image)
        ud.set(name, forKey: "name"+numberSelected)
        ud.set(number, forKey: "phone"+numberSelected)
    }
    
    @IBAction func addClicked(_ sender: Any) {
        print( "name"+numberSelected)
        savePhoneDetails(name: contactName.text!, number: contactNumber.text!)
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
extension AddPhoneDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        guard var image = info[.editedImage] as? UIImage else { return }
        imageView.image = image
    }
}
