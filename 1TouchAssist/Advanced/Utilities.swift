//
//  Utilities.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/16/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}

func directoryExistsAtPath(_ path: String) -> Bool {
    var isDirectory = ObjCBool(true)
    let exists = FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory)
    return exists && isDirectory.boolValue
}

func makeCall(with number: String, vc: UIViewController) {
    guard let url = URL(string: "telprompt://\(number)") else { return }
    if UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    } else {
        let ac = UIAlertController(title: "Uh Oh...", message: "The phone number isnt valid... edit and try again", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Okay", style: .default, handler: nil)
        ac.addAction(ok)
        vc.present(ac, animated: true, completion: nil)
    }
}
