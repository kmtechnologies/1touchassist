//
//  CirlceButton.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/15/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

class CirlceButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.tintColor = .white
        self.setTitleColor(.white, for: .normal)
        }

}
