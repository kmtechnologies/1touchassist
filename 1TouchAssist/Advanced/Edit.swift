//
//  Edit.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/17/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

class Edit {
    static let shared = Edit()
    
    var editing: Bool? = false
    
    var contactName: String?
    var contactNumber: String?
    var image: Data?
}

class EditSubtask {
    static let shared = EditSubtask()
    
    var editing: Bool? = false
    
    var title: String?
}
