//
//  Functions.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/19/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UserNotifications
import UIKit

class Notifications {
    class func maintask() {
        if jsonTodoArray.count != 0 {
        var alertDate = DateComponents()
        alertDate.hour = 12
        alertDate.minute = 0
        let content = UNMutableNotificationContent()
        content.sound = .default
        content.title = "You have tasks that aren't completed yet!"
            
        let trigger = UNCalendarNotificationTrigger(dateMatching: alertDate, repeats: true)
            let request = UNNotificationRequest(identifier: "taskNotCompleted", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        } else if jsonTodoArray.count == 0 {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        }
        else {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        }
    }
}
