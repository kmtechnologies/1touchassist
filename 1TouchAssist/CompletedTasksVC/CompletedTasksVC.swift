//
//  CompletedTasksVC.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/19/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

class CompletedTasksVC: UITableViewController {
    
    var array = completedTasks.sorted(by: { $0.dateCompleted.compare($1.dateCompleted) == .orderedDescending })


    override func viewDidLoad() {
        print(array)
        super.viewDidLoad()
        title = "Completed Tasks"
        if completedTasks.count >= 1 {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(clearCompleted))
        } else {
            
        }
        

    }
    
    @objc func clearCompleted() {
        completedTasks.removeAll()
        array.removeAll()
        saveCompletedTasks()
        DispatchQueue.main.async {
            UIView.transition(with: self.tableView, duration: 0.4, options: .transitionCrossDissolve, animations: { self.tableView.reloadData() ; self.navigationItem.rightBarButtonItem = nil }, completion: nil)
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if array[section].opened == true {
            return array[section].subtasks.count + 1
        } else {
            return 1
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return array.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "completedTaskCell", for: indexPath)
            switch array[indexPath.section].color {
            case "Red":
                cell.backgroundColor = UIColor(named: "todoRed")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            case "Blue":
                cell.backgroundColor = UIColor(named: "todoBlue")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            case "Green":
                cell.backgroundColor = UIColor(named: "todoGreen")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            case "Purple":
                cell.backgroundColor = UIColor(named: "todoPurple")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            default:
                cell.backgroundColor = .white
                cell.textLabel?.textColor = .black
            }
            cell.textLabel?.text = array[indexPath.section].title
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy - h:mm aa"
//            formatter.timeZone = TimeZone(abbreviation: "CDT")
            formatter.locale = .current
            let dateString = "Completed On: " +  formatter.string(from: array[indexPath.section].dateCompleted)
            cell.detailTextLabel?.text = dateString
            cell.tintColor = .white
            cell.accessoryType = .disclosureIndicator
            return cell
        } else {
             let cell = tableView.dequeueReusableCell(withIdentifier: "completedSubtaskCell", for: indexPath)
            switch array[indexPath.section].color {
            case "Red":
                cell.backgroundColor = UIColor(named: "subtaskRed")
                cell.textLabel?.textColor = .white
                
            case "Blue":
                cell.backgroundColor = UIColor(named: "subtaskBlue")
                cell.textLabel?.textColor = .white
            case "Green":
                cell.backgroundColor = UIColor(named: "subtaskGreen")
                cell.textLabel?.textColor = .white
            case "Purple":
                cell.backgroundColor = UIColor(named: "subtaskPurple")
                cell.textLabel?.textColor = .white
            default:
                cell.backgroundColor = .white
                cell.textLabel?.textColor = .black
            }
            cell.textLabel?.text = array[indexPath.section].subtasks[indexPath.row - 1].title
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy"
            let dateString = "Completed On: " +  formatter.string(from: array[indexPath.section].dateCompleted)
            cell.detailTextLabel?.text = dateString
            cell.detailTextLabel?.textColor = .white
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if array[indexPath.section].opened == true {
            array[indexPath.section].opened = false
            saveCompletedTasks()
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        } else {
            array[indexPath.section].opened = true
            saveCompletedTasks()
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
        
        if let indexPath = tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
 

}
