//
//  TodoData.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/12/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

struct TodoList: Encodable, Decodable {
    var todo: [TodoItem]
}

struct TodoItem: Encodable, Decodable {
    var title: String
    var color: String
    var message: String
    var date: Date
    var subtasks: [Subtask]
    var opened: Bool = false
    var completed: Bool? = false
}

var jsonTodoArray: [TodoItem] = []

func loadJson() {
    let pathDirectory = getDocumentsDirectory()
    let filePath = pathDirectory.appendingPathComponent("todoList.json")
        do {
            let data = try Data(contentsOf: filePath)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode([TodoItem].self, from: data)
            jsonTodoArray = jsonData
        } catch {
            print(error)
        }
}

func saveJson() {
    let pathDirectory = getDocumentsDirectory()
    let filePath = pathDirectory.appendingPathComponent("todoList.json")
    let json = try? JSONEncoder().encode(jsonTodoArray)
    do {
        try json!.write(to: filePath)
    } catch {
        print(error)
    }
}

struct completedTask: Codable {
    var title: String
    var color: String
    var message: String
    var date: Date
    var subtasks: [Subtask]
    var opened: Bool = false
    var completed: Bool? = false
    var dateCompleted: Date
}

var completedTasks: [completedTask] = []


func saveCompletedTasks() {
    let pathDirectory = getDocumentsDirectory()
    let filePath = pathDirectory.appendingPathComponent("completedTasks.json")
    let json = try? JSONEncoder().encode(completedTasks)
    do {
        try json!.write(to: filePath)
    } catch {
        print(error)
    }
}

func loadCompletedTasks() {
    let pathDirectory = getDocumentsDirectory()
    let filePath = pathDirectory.appendingPathComponent("completedTasks.json")
    do {
        let data = try Data(contentsOf: filePath)
        let decoder = JSONDecoder()
        let jsonData = try decoder.decode([completedTask].self, from: data)
        completedTasks = jsonData
    } catch {
        print(error)
    }
}
