//
//  TodoVC.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/12/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

var selectedItem: TodoItem?
var selectedIndex: Int?

var compAlreadyExe: Bool = false

class TodoVC: UIViewController {
    
    var newItem: TodoItem? = nil
    
    @IBOutlet var todoTable: UITableView!
    @IBOutlet weak var blurView: UIVisualEffect!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func goBack(_ sender: Any) {
        let sb = UIStoryboard.init(name: "HomeVC", bundle: nil)
        let vc = sb.instantiateInitialViewController()
        present(vc!, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        guard let reloadIndex = reloadIndex else { return }
        
        DispatchQueue.main.async {
            UIView.transition(with: self.todoTable, duration: 0.35, options: .transitionCrossDissolve, animations: {
                self.todoTable.beginUpdates()
                self.todoTable.reloadData()
                self.todoTable.endUpdates()
            }, completion: nil)
        }
    }
    
    @IBAction func addTodo(_ sender: Any) {
        let sb = UIStoryboard(name: "AddTodoVC", bundle: nil)
        let vc = sb.instantiateInitialViewController()!
        present(vc, animated: true, completion: nil)
    }
}
extension TodoVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if jsonTodoArray[section].opened == true {
            return jsonTodoArray[section].subtasks.count + 1
        } else {
            return 1
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if indexPath.row == 0 {
            let cell = todoTable.dequeueReusableCell(withIdentifier: "todoCell", for: indexPath)
            switch jsonTodoArray[indexPath.section].color {
            case "Red":
                cell.backgroundColor = UIColor(named: "todoRed")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            case "Blue":
                cell.backgroundColor = UIColor(named: "todoBlue")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            case "Green":
                cell.backgroundColor = UIColor(named: "todoGreen")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            case "Purple":
                cell.backgroundColor = UIColor(named: "todoPurple")
                cell.textLabel?.textColor = .white
                cell.detailTextLabel?.textColor = .white
            default:
                cell.backgroundColor = .white
                cell.textLabel?.textColor = .black
                cell.detailTextLabel?.textColor = .black
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy - h:mm aa"
//            formatter.timeZone = TimeZone(abbreviation: "CDT")
            formatter.locale = .current
            let string = "Complete By: " +  formatter.string(from: jsonTodoArray[indexPath.section].date)
            cell.textLabel?.text = jsonTodoArray[indexPath.section].title
            cell.accessoryType = .detailButton
            cell.tintColor = .white
            cell.detailTextLabel?.text = string
            return cell
        } else {
            let cell = todoTable.dequeueReusableCell(withIdentifier: "subtaskCell", for: indexPath)
            switch jsonTodoArray[indexPath.section].color {
            case "Red":
                cell.backgroundColor = UIColor(named: "subtaskRed")
                cell.textLabel?.textColor = .white

            case "Blue":
                cell.backgroundColor = UIColor(named: "subtaskBlue")
                cell.textLabel?.textColor = .white
            case "Green":
                cell.backgroundColor = UIColor(named: "subtaskGreen")
                cell.textLabel?.textColor = .white
            case "Purple":
                cell.backgroundColor = UIColor(named: "subtaskPurple")
                cell.textLabel?.textColor = .white
            default:
                cell.backgroundColor = .white
                cell.textLabel?.textColor = .black
            }
            if jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].completed == true {
                cell.accessoryType = .checkmark
                cell.isUserInteractionEnabled = false
                cell.contentView.alpha = 0.5
                let attr: [NSAttributedString.Key : Any?] = [NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.strikethroughColor : UIColor.black]
                let textLabel = NSAttributedString(string: jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].title, attributes: attr as [NSAttributedString.Key : Any])
                cell.textLabel?.attributedText = textLabel
                
            } else {
                cell.accessoryType = .none
                cell.contentView.alpha = 1.0
                cell.textLabel?.text = jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].title
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let ac = UIAlertController(title: "Details", message: jsonTodoArray[indexPath.section].message, preferredStyle: .alert)
        let okay = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        ac.addAction(okay)
        present(ac, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let complete = UIContextualAction(style: .normal, title: "Complete") { (action, view, completion) in
            
            if indexPath.row >= 1 {
                let subtask = jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].title
                jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].completed = true
                let cell = self.todoTable.cellForRow(at: indexPath)
                if jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].completed == true {
                    cell?.accessoryType = .checkmark
                    let attr: [NSAttributedString.Key : Any?] = [NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.strikethroughColor : UIColor.black]
                    let textLabel = NSAttributedString(string: jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].title, attributes: attr as [NSAttributedString.Key : Any])
                    cell?.textLabel?.attributedText = textLabel
                    cell?.contentView.alpha = 0.5
                } else {
                    cell?.contentView.alpha = 1.0
                }
                saveJson()
                _ = SweetAlert().showAlert("Congrats!", subTitle: "You completed the subtask - \(subtask)", style: .success, buttonTitle: "Yay!")
                completion(true)
            } else {
                
                var todo = jsonTodoArray[indexPath.section]
                
                let compTrue = {
                    let completed = completedTask(title: todo.title, color: todo.color, message: todo.message, date: todo.date, subtasks: todo.subtasks, opened: false, completed: true, dateCompleted: Date())
                    completedTasks.append(completed)
                    jsonTodoArray.remove(at: indexPath.section)
                    let sections = IndexSet(integer: indexPath.section)
                    tableView.deleteSections(sections, with: .automatic)
                    saveCompletedTasks()
                    saveJson()
                    _ = SweetAlert().showAlert("Congrats!", subTitle: "You completed the task - \(todo.title)", style: .success, buttonTitle: "Yay!")
                    completion(true)
                }
    
                let compFalse = {
                    _ = SweetAlert().showAlert("Uh oh...", subTitle: "You have subtasks that arent completed...Are you sure you wanted to complete this task?", style: .warning, buttonTitle: "No", buttonColor: UIColor(named: "todoRed")! , otherButtonTitle: "Yes", otherButtonColor: UIColor(named: "todoGreen")! , action: { (completeAction) in
                        if completeAction == true {
                            completion(false)
                            compAlreadyExe = false
                        } else {
                            compTrue()
                            compAlreadyExe = false
                        }
                        return
                    })
                }
                
                
                let check = {
                    
                    if jsonTodoArray[indexPath.section].subtasks.allSatisfy({$0.completed == true}) {
                        compTrue()
                    } else {
                        if !compAlreadyExe {
                            compFalse()
                            compAlreadyExe = true
                        }
                    }
                    
                }
                
                if jsonTodoArray[indexPath.section].subtasks.count == 0 {
                    compTrue()
                } else if jsonTodoArray[indexPath.section].subtasks.count >= 1{
                    check()
                }
            }
        }
        let add = UIContextualAction(style: .normal, title: "Add") { (add, view, completion) in
            completion(true)
            if indexPath.row == 0 {
                print(indexPath.section)
                selectedIndex = indexPath.section
                subtaskIndex = indexPath.row
                let sb = UIStoryboard(name: "AddSubtasksVC", bundle: nil)
                guard let vc = sb.instantiateInitialViewController() else { return }
                self.present(vc, animated: true, completion: nil)
            } else {
                print("cant do this")
            }
        }
        
        add.backgroundColor = UIColor(named: "addColor")
        complete.backgroundColor = UIColor(named: "completeColor")
        
        if indexPath.row == 0 {
            return UISwipeActionsConfiguration(actions: [complete, add])
        } else {
            return UISwipeActionsConfiguration(actions: [complete])
        }
        
    }
    
    @IBAction func showCompletedTasks(_ sender: Any) {
        self.performSegue(withIdentifier: "toCompleted", sender: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return jsonTodoArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if jsonTodoArray[indexPath.section].opened == true {
            jsonTodoArray[indexPath.section].opened = false
            saveJson()
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        } else {
            jsonTodoArray[indexPath.section].opened = true
            saveJson()
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
        
        if let indexPath = todoTable.indexPathForSelectedRow{
            self.todoTable.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let todo = jsonTodoArray[indexPath.section].title
        
        
        let action = UIContextualAction(style: .normal, title: "Delete") { (action, view, completion) in
            
            if indexPath.row >= 1 {
                let subtask = jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].title
                SweetAlert().showAlert("Delete?", subTitle: "Are you sure you want to delete the task - \(subtask)", style: .warning, buttonTitle: "Cancel", buttonColor: .gray, otherButtonTitle: "Delete it", otherButtonColor: .red, action: { (action) in
                    if action == true {
                        completion(false)
                    } else {
                        jsonTodoArray[indexPath.section].subtasks.remove(at: indexPath.row - 1)
                        self.todoTable.deleteRows(at: [indexPath], with: .automatic)
                        saveJson()
                        _ = SweetAlert().showAlert("Complete", subTitle: "You have deleted the task - \(subtask)", style: .success, buttonTitle: "Okay")
                        completion(true)
                    }
                })
            } else {
                SweetAlert().showAlert("Delete?", subTitle: "Are you sure you want to delete the task - \(todo)", style: .warning, buttonTitle: "Cancel", buttonColor: .gray, otherButtonTitle: "Delete it", otherButtonColor: .red, action: { (action) in
                    
                    if action == true {
                        completion(false)
                    } else {
                        jsonTodoArray.remove(at: indexPath.section)
                        let sections = IndexSet(integer: indexPath.section)
                        tableView.deleteSections(sections, with: .automatic)
                        saveJson()
                        _ = SweetAlert().showAlert("Complete", subTitle: "You have deleted the task - \(todo)", style: .success, buttonTitle: "Okay")
                        completion(true)
                    }
                })
            }
        }
        
        let edit = UIContextualAction(style: .normal, title: "Edit") { (edit, view, completion) in
            if indexPath.row >= 1 {
                let edit = EditSubtask.shared
                edit.editing = true
                edit.title = jsonTodoArray[indexPath.section].subtasks[indexPath.row - 1].title
                selectedIndex = indexPath.section
                subtaskIndex = indexPath.row
                let sb = UIStoryboard(name: "AddSubtasksVC", bundle: nil)
                guard let vc = sb.instantiateInitialViewController() else { return }
                self.present(vc, animated: true, completion: nil)
                completion(true)
            } else {
                selectedIndex = indexPath.section
                selectedItem = jsonTodoArray[indexPath.section]
                self.performSegue(withIdentifier: "toDetails", sender: self)
                completion(true)
            }
        }
        
        edit.backgroundColor = UIColor(named: "editColor")
        
        action.backgroundColor = UIColor(named: "deleteColor")
        
        return UISwipeActionsConfiguration(actions: [action, edit])
    }
    
}
