//
//  AddTodoVC.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/16/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

class AddTodoVC: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var colorSelect: UISegmentedControl!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
//        textView.text = "Placeholder"
        messageText.textColor = UIColor.lightGray
        messageText.delegate = self
        mainView.layer.cornerRadius = 10
        mainView.layer.borderColor = UIColor.white.cgColor
        mainView.layer.borderWidth = 1
        messageText.layer.cornerRadius = 10
        addButton.isHidden = true
        datePicker.minimumDate = Date()
        datePicker.setValue(UIColor.white, forKey: "textColor")
        }
    
    @IBAction func editChnaged(_ sender: Any) {
        if titleText.text != "" {
            addButton.isHidden = false
        } else {
            addButton.isHidden = true
        }
    }
    
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToTable" {
            let item = TodoItem(title: titleText.text!, color: colorSelect.titleForSegment(at: colorSelect.selectedSegmentIndex)!, message: messageText.text, date: datePicker.date, subtasks: [], opened: false, completed: false)
            jsonTodoArray.append(item)
            saveJson()
        }
    }

}
extension AddTodoVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Item Comments..."
            textView.textColor = UIColor.lightGray
        }
    }
}
