//
//  SettingsVC.swift
//  1TouchAssist
//
//  Created by KM Technologies. on 4/15/19.
//  Copyright © 2019 KM Technologies. All rights reserved.
//

import UIKit

class SettingsVC: UITableViewController {
    
    let aVersion        = "CFBundleShortVersionString"
    let aBuildNumber    = "CFBundleVersion"
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var labelSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        versionLabel.text = getVersion()
        if ud.bool(forKey: "tabbarLabels") == true {
            labelSwitch.isOn = true
        } else {
            labelSwitch.isOn = false
        }
    }
    @IBAction func labelSwitchChanged(_ sender: UISwitch) {
        if sender.isOn {
            ud.set(true, forKey: "tabbarLabels")
        } else {
            ud.set(false, forKey: "tabbarLabels")
        }
    }
    
    @IBAction func backToHome(_ sender: Any) {
        let sb = UIStoryboard.init(name: "HomeVC", bundle: nil)
        let vc = sb.instantiateInitialViewController()
        present(vc!, animated: true, completion: nil)
    }
    func getVersion() -> String {
        let dictionary  = Bundle.main.infoDictionary!
        let version     = dictionary[aVersion] as! String
        let build       = dictionary[aBuildNumber] as! String
        
        return "\(version) (\(build))"
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            //JARED PUT YOUR EMAIL AFTER YOU NAME BY ADDING \N YOUR EMAIL JUST LIKE I DID FOR ME
            SweetAlert().showAlert("About", subTitle: "Sponsor/Developer:\n Jared Walther - OSO Unlimited LLC\n\nAssistant Developer:\n Kyle Mendell - iOS Developer\nkmtechnologies84@gmail.com", style: .none, buttonTitle: "Okay")
        } else if indexPath.row == 2 {
            SweetAlert().showAlert("Credits", subTitle: "SweetAlert\nhttps://github.com/codestergit/SweetAlert-iOS\n\niOS Glyph Pack\nhttps://icons8.com/ios\n\nImage Credits:\nBackground Image\nhttps://wallpaperiphone2018.com/iphone-wallpaper-quad-hd/iphone-wallpaper-quad-hd-awesome-car-audi-drive-interior-motor-man-iphone-7-wallpaper/\n\nYoutube Icon\nhttps://commons.wikimedia.org/wiki/File:YouTube_social_red_circle_(2017).svg\n\nSpotify Icon\nhttps://airfreshener.club/quotes/linkedin-logo-clear-background.html\n\nYT Music Icon\nhttps://commons.wikimedia.org/wiki/File:YouTubeMusic_Logo.png\n\nPodcast Icon\nhttps://www.iconfinder.com/icons/287539/podcast_icon/n/n", style: .none, buttonTitle: "Okay")
        }
        
        if let indexPath = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}
